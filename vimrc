set sts=2 sw=2 et
set guioptions-=m
set guioptions-=T

let g:rainbow_active = 1 "doesn't work? why?
autocmd BufWritePost *.py call Flake8()
